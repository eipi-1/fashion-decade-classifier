from fastai import *
from flask import Flask, render_template, jsonify, request


app = Flask(__name__)


@app.route("/hello")
def hello():
    return "Hello"


# @app.route("/classify-url", methods=["GET"])
# def classify_url():
#     bytes = get_bytes(request.query_params["url"])
#     img = open_image(BytesIO(bytes))
#     _, _, losses = learner.predict(img)
#     return JSONResponse(
#         {
#             "predictions": sorted(
#                 zip(cat_learner.data.classes, map(float, losses)),
#                 key=lambda p: p[1],
#                 reverse=True,
#             )
#         }
#     )

